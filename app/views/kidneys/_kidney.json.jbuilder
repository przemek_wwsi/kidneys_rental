json.extract! kidney, :id, :name, :value, :description, :blood_group, :fresh_harvest, :created_at, :updated_at
json.url kidney_url(kidney, format: :json)
