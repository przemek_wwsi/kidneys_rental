class KidneysController < ApplicationController
  before_action :set_kidney, only: [:show, :edit, :update, :destroy]

  # GET /kidneys
  # GET /kidneys.json
  def index
    @kidneys = Kidney.all
  end

  # GET /kidneys/1
  # GET /kidneys/1.json
  def show
  end

  # GET /kidneys/new
  def new
    @kidney = Kidney.new
  end

  # GET /kidneys/1/edit
  def edit
  end

  # POST /kidneys
  # POST /kidneys.json
  def create
    @kidney = Kidney.new(kidney_params)

    respond_to do |format|
      if @kidney.save
        format.html { redirect_to @kidney, notice: 'Kidney was successfully created.' }
        format.json { render :show, status: :created, location: @kidney }
      else
        format.html { render :new }
        format.json { render json: @kidney.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kidneys/1
  # PATCH/PUT /kidneys/1.json
  def update
    respond_to do |format|
      if @kidney.update(kidney_params)
        format.html { redirect_to @kidney, notice: 'Kidney was successfully updated.' }
        format.json { render :show, status: :ok, location: @kidney }
      else
        format.html { render :edit }
        format.json { render json: @kidney.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kidneys/1
  # DELETE /kidneys/1.json
  def destroy
    @kidney.destroy
    respond_to do |format|
      format.html { redirect_to kidneys_url, notice: 'Kidney was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kidney
      @kidney = Kidney.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kidney_params
      params.require(:kidney).permit(:name, :value, :description, :blood_group, :fresh_harvest)
    end
end
