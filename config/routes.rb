Rails.application.routes.draw do
  resources :kidneys
  devise_for :users

  root 'home#index'
end
