SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.item :kidneys, 'Kidneys', kidneys_path
    primary.item :logout, 'Logout', destroy_user_session_path, method: :delete,
                 html: { style: 'float:right' }
  end
end