class CreateKidneys < ActiveRecord::Migration[5.2]
  def change
    create_table :kidneys do |t|
      t.string :name
      t.decimal :value
      t.text :description
      t.string :blood_group
      t.boolean :fresh_harvest

      t.timestamps
    end
  end
end
